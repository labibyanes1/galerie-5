# peinture_app
peinture_app est un site internet de presentation des peintures


## Environnement de developpement


### Pré-requis
PHP 7.4
Composer
Symfony CLI
Docker
Docker-compose 
node js et npm

Vous pouvez verifier les pré-requis (sauf Docker et Docker-compose) avac la commande 
de la CLI symfony
''bash
symfony check:requirements
''


## Lancer l'environnement de développement

'''bash
composer install
docker-compose up -d
npm install
npm run build
symfony serve -d
'''	


## Lancer les tests
'''bash
php bin/phpunit --testdox
'''

## ajout des fake data a travers fakerphp

symfony console doctrine:fixture:load
