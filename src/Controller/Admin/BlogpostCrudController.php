<?php

namespace App\Controller\Admin;

use App\Entity\Blogpost;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class BlogpostCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blogpost::class;
    }
    # public function configureFields(string $pageName): iterable
    #  {
    #    return [
    # public function configureFields(string $pageName): iterable
    # {
    #    return [
    #       TextField::new('titre'),
    #      TextField::new('slug'),
    #     TextareaField::new('contenu'),
    #    DateField::new('createdAt'),
    # ];
    # }



    #  ];
    #   }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setDefaultSort(['id' => 'DESC'])
        ->setPageTitle('index', 'Actualitès');
    }
}
