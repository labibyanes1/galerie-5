<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AproposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="a-propos")
     */
    public function index(UserRepository $userReposritory): Response
    {
        return $this->render('a-propos/index.html.twig', [
            'peintre' => $userReposritory->getPeintre(),
        ]);
    }
}
