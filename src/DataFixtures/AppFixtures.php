<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Blogpost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        // utilisation du faker
        $faker = Factory::create('fr_FR');
        // $product = new Product();
        // $manager->persist($product);

        //creation de 10 utilisateur
      // for( $i=0 ; $i<10 ; $i++){
            $user = new User();

            $user->setEmail($faker->email())
             ->setPrenom($faker->firstName())
             ->setNom($faker->lastName())
             ->setPhone($faker->phoneNumber())
             ->setaPropos($faker->text())
             ->setInstagram('instagram')
             ->setRoles(['ROLE_PEINTRE']);

            $password = $this->encoder->encodePassword($user, 'password');
            $user->setPassword($password);

            $manager->persist($user);
       //}
        $manager->flush();
    }
}
